---
layout: post
title:  WikiBit Airdrop
date: 2020-01-02 19:20:23 +0900
category: airdropEN
---
#  WikiBit

__WikiBit__ is a project that establishes an authentic and reliable credit database system with wide coverage by connecting various credit data in series based on big-data multidimensional risk assessment and blockchain technology. Credit data was shared through blockchain alliance to create an intelligent digital credit platform. Meanwhile, WikiBit project team promises to reverse repurchase coins globally to make WikiBit the preferred credit currency in the world. [1](https://www.wikibit.cc/kKIgHllvOw)


