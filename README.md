---
layout: page
title: About
permalink: /about/
---

## EN

## _What is this about_

This blog is just my personal notes about crypto assets.

### You are free to:
__Share__ — copy and redistribute the material in any medium or format

__Adapt__ — remix, transform, and build upon the material for any purpose, even commercially.
￼
The licensor cannot revoke these freedoms as long as you follow the license terms.

### Under the following terms:
__Attribution__ — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

__No additional restrictions__ — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

## PL 

## _O czym będzie blog_

Ten blog to po prostu udotępnione moje notatki o kryptowalutach.

### Masz pełne pozwolenie na:
__Dzielenie się__ — kopiuj i rozpowszechniaj utwór w dowolnym medium i formacie

__Adaptacje__ — remiksuj, zmieniaj i twórz na bazie utworu dla dowolnego celu, także komercyjnego.
￼
Licencjodawca nie może odwołać udzielonych praw, o ile są przestrzegane warunki licencji.

### Na następujących warunkach:

__Uznanie autorstwa__ — Utwór należy odpowiednio oznaczyć, podać link do licencji i wskazać jeśli zostały dokonane w nim zmiany . Możesz to zrobić w dowolny, rozsądny sposób, o ile nie sugeruje to udzielania prze licencjodawcę poparcia dla Ciebie lub sposobu, w jaki wykorzystujesz ten utwór.

